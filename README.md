# Summary

# Usage
Learn more about each script below and try some out!

# impact_check.py (W.I.P.)

## Summary
A script that attempts to idempotently configure and run an impact check at https://copr.fedorainfracloud.org/.
Given some package information, it will find related dependencies, create main and control coprs, add and build
the related dependencies in each, and display the failures associated with each copr.
This script can be run any number of times and it will only configure or build what it needs based on the state
of the coprs.

## Usage
```
python impact_check.py --name python-CacheControl --distname python3-CacheControl --branch rebase-0.12.14
```

## Example Run

1st Attempt
```
python impact_check.py --name python-CacheControl --distname python3-CacheControl --branch rebase-0.12.14

(For reference) Direct dependencies for python-CacheControl:
poetry

(For builds) Transitive dependencies for python-CacheControl:
git-up
nanopb
noggin
ocaml-dose3
poetry
python-aioeafm
python-aioflo
python-aionotion
python-niaclass
python-pathable
python-poetry-plugin-export
python-pyairnow
python-pyairvisual
python-pyopenuv
python-pytest-spec
python-pytile
python-pytzdata
python-sklearn-nature-inspired-algorithms
python-stochastic
stomppy

Creating COPR: autoimpact-python-CacheControl

Creating COPR: autoimpact-python-CacheControl-control

Adding primary package (python-CacheControl) via distgit.

Found 20 dependencies to add to the COPR:
git-up
nanopb
noggin
ocaml-dose3
poetry
python-aioeafm
python-aioflo
python-aionotion
python-niaclass
python-pathable
python-poetry-plugin-export
python-pyairnow
python-pyairvisual
python-pyopenuv
python-pytest-spec
python-pytile
python-pytzdata
python-sklearn-nature-inspired-algorithms
python-stochastic
stomppy

Building primary package (python-CacheControl) via distgit.

Command Failure:
  copr monitor --output json autoimpact-python-CacheControl --dirname=autoimpact-python-CacheControl:custom:isolated
  ->
  Something went wrong:
Error: Dirname 'autoimpact-python-CacheControl:custom:isolated' doesn't exist in 'rerickso/autoimpact-python-CacheControl' copr
Found 20 packages to build in the COPR:
git-up
nanopb
noggin
ocaml-dose3
poetry
python-aioeafm
python-aioflo
python-aionotion
python-niaclass
python-pathable
python-poetry-plugin-export
python-pyairnow
python-pyairvisual
python-pyopenuv
python-pytest-spec
python-pytile
python-pytzdata
python-sklearn-nature-inspired-algorithms
python-stochastic
stomppy
Command Failure:
  copr build-package autoimpact-python-CacheControl:custom:isolated --name python-poetry-plugin-export --background --nowait
  ->
  Something went wrong:
Error: Response is not in JSON format, there is probably a bug in the API code.
Try 'copr-cli --debug' for more info.
Command Failure:
  copr build-package autoimpact-python-CacheControl:custom:isolated --name poetry --background --nowait
  ->
  Something went wrong:
Error: Response is not in JSON format, there is probably a bug in the API code.
Try 'copr-cli --debug' for more info.
Command Failure:
  copr build-package autoimpact-python-CacheControl:custom:isolated --name python-aionotion --background --nowait
  ->
  Something went wrong:
Error: Response is not in JSON format, there is probably a bug in the API code.
Try 'copr-cli --debug' for more info.
FAILED TO BUILD/START BUILD FOR: poetry
FAILED TO BUILD/START BUILD FOR: python-aionotion
FAILED TO BUILD/START BUILD FOR: python-poetry-plugin-export

[Attempt #0] Incomplete builds remain. Sleeping for 3 minutes.
[Attempt #1] Incomplete builds remain. Sleeping for 7 minutes.

Failed Builds:
git-up

Found 1 failures to build in the control COPR:
git-up

[Attempt #0] Incomplete builds remain. Sleeping for 3 minutes.
[Attempt #1] Incomplete builds remain. Sleeping for 7 minutes.

Control COPR Failed Builds:
git-up

Failed on both, likely not due to your changes:
git-up

Failed on latest, Passed on control:

Impact check passed - no anomalies found!
```

2nd Attempt

```
python impact_check.py --name python-CacheControl --distname python3-CacheControl --branch rebase-0.12.14

(For reference) Direct dependencies for python-CacheControl:
poetry

(For builds) Transitive dependencies for python-CacheControl:
git-up
nanopb
noggin
ocaml-dose3
poetry
python-aioeafm
python-aioflo
python-aionotion
python-niaclass
python-pathable
python-poetry-plugin-export
python-pyairnow
python-pyairvisual
python-pyopenuv
python-pytest-spec
python-pytile
python-pytzdata
python-sklearn-nature-inspired-algorithms
python-stochastic
stomppy

COPR Exists, skipping creation: autoimpact-python-CacheControl

COPR Exists, skipping creation: autoimpact-python-CacheControl-control

Primary package (python-CacheControl) was already successfully added.


Primary package (python-CacheControl) was already successfully built. To force a rebuild, provide the --rebuild flag (srpm case) or push updates to the given repo (dist-git case).

Found 1 packages to build in the COPR:
git-up

[Attempt #1] Incomplete builds remain. Sleeping for 5.0 minutes.
 
Failed Builds:
git-up

Found 1 failures to build in the control COPR:
git-up
Command Failure:
  copr add-package-distgit autoimpact-python-CacheControl-control --name git-up --commit rawhide --webhook-rebuild on
  ->
  Something went wrong:
Error: Package git-up already exists in copr rerickso/autoimpact-python-CacheControl-control.
FAILED TO ADD VIA DISTGIT: git-up

[Attempt #1] Incomplete builds remain. Sleeping for 5.0 minutes.

Control COPR Failed Builds:
git-up

Failed on both, likely not due to your changes:
git-up

Failed on latest, Passed on control:

Impact check passed - no anomalies found!
```

# get_build_errors.py

## Summary
A script to quickly generate and filter through copr repo build logs
NOTE: Here there be plenty of false positives and ineffeciencies!

## Usage
Inputs:
- [required] `-c | --copr` The name of your copr repo
- [optional] `-d | --dirname` A copr directory to monitor
- [optional] `-f | --failures` A failures file to filter the results by
  (standard newline separated {package name} or {name}=={version} tokens)
- [optional] `-o | --outdir` A directory to output result files to

Outputs:
- `/tmp/{copr}_builds.json` # All copr monitor data
- `/tmp/{copr}_failures.json` # All failures from the copr monitor data
- `/tmp/{copr}_errors.txt` # All filtered error output

Examples:
- `python get_build_errors.py --copr python-markupsafe-2.1.3`
- `python get_build_errors.py -c python-markupsafe-2.1.3 -f failures.txt`

## Example Run
```
$ python get_build_errors.py -c $COPR -d $COPR:custom:isolated

Builds:
/tmp/python-requests-2.31.0_builds.json

Failures:
/tmp/python-requests-2.31.0_failures.json

Errors:
/tmp/python-requests-2.31.0_errors.txt


$ less /tmp/python-requests-2.31.0_errors.txt

...

################################################################################

# PACKAGE #
python-CacheControl==0.12.11-4
https://download.copr.fedorainfracloud.org/results/rerickso/python-requests-2.31.0:custom:isolated/fedora-rawhide-x86_64/06087440-python-CacheControl/builder-live.log.gz

# KNOWN ERRORS #
=========================== short test summary info ============================
FAILED tests/test_etag.py::TestReleaseConnection::test_not_modified_releases_connection
=========== 1 failed, 99 passed, 1 deselected, 44 warnings in 0.42s ============

# POSSIBLE ERRORS #
# N/A - SEE ABOVE #

################################################################################

# PACKAGE #
python-nose2==0.13.0-1
https://download.copr.fedorainfracloud.org/results/rerickso/python-requests-2.31.0:custom:isolated/fedora-rawhide-x86_64/06066613-python-nose2/builder-live.log.gz

# KNOWN ERRORS #


# POSSIBLE ERRORS #
    raise PackageDiscoveryError(cleandoc(msg))
setuptools.errors.PackageDiscoveryError: Multiple top-level packages discovered in a flat-layout: ['nose2', 'SPECPARTS'].
ERROR: Exception(/var/lib/copr-rpmbuild/results/python-nose2-0.13.0-1.fc39.src.rpm) Config(fedora-rawhide-x86_64) 1 minutes 13 seconds
ERROR: Command failed: 

################################################################################

...

```
