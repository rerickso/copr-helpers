"""
A script to quickly generate and filter through copr repo build logs
NOTE: Here there be plenty of false positives and ineffeciencies!

Inputs:
- [required] -c | --copr The name of your copr repo
- [optional] -f | --failures A failures file to filter the results by
  (standard newline separated {package name} or {name}=={version} tokens)

Outputs:
- outputs/{copr}_builds.json # All copr monitor data
- outputs/{copr}_failures.json # All failures from the copr monitor data
- outputs/{copr}_errors.txt # All filtered error output

Examples:
- python get_build_errors.py --copr python-markupsafe-2.1.3
- python get_build_errors.py -c python-markupsafe-2.1.3 -f failures.txt
"""

import argparse
import json
import os
import re
import requests
import subprocess
from textwrap import dedent
from time import sleep

ERROR_LINE_MATCH = re.compile('\bfatal|Error|ERROR|Failed|FAILED\b')
KNOWN_ERROR_MATCHES = [
    re.compile(r'Error: Transaction test error:.*?\n\n', re.S),
    re.compile(r'Error: Failed to download metadata for repo.*?\n'),
    # dnf unable to download a package - list effected packages
    re.compile(r'Error: Error downloading packages:\n(\s+.*?\n)*', re.S),
    # dnf package conflicts
    re.compile(
        r'Error:\s*\n\s*Problem[\s\d]*:.*?\(try to add \'--skip-broken\' \
        to skip uninstallable packages or \'--nobest\' to use not only best candidate packages\)',
        re.S
    ),
    # general python exceptions
    re.compile(r'Exception occurred.*?\n\n', re.S),
    # pyetest summary w/failed or errors
    re.compile(r'={3,}\sshort test summary info\s={3,}.*={3,}.*\d+\s(?:failed|error[s]*).*={3,}', re.S),
    # nose summary w/failures
    re.compile(r'={3,}\nFAIL:\s.*?FAILED \(failures=\d+.*?\)', re.S),
    # common c error ex. header file imports
    re.compile(r'.*fatal error.*'),
    # COPR timeout
    re.compile(r'.*Copr timeout.*'),
]
MAX_ERROR_LINE_LENGTH = 256
NL = "\n"
SEP = "#" * 80


def get_args():
    parser = argparse.ArgumentParser(
        prog="python get_build_errors.py",
        description="Quickly generate and filter through copr repo build logs",
        epilog="Outputs to (default) /tmp/{copr}_{builds,failures,errors}.{json,txt}"
    )
    parser.add_argument('-c', '--copr', action='store', help="The name of your copr repo", required=True)
    parser.add_argument('-d', '--dirname', action='store', help="A copr directory to monitor")
    f_help = "A failures file to filter the results by (standard newline separated {package name} or {name}=={version} tokens)"
    parser.add_argument('-f', '--failures', action='store', help=f_help, default=None)
    parser.add_argument('-o', '--outdir', action='store', help="A directory to output result files to.", default='/tmp')

    return parser.parse_args()


def validate_args(args):
    if not (os.path.exists(args.outdir) and os.path.isdir(args.outdir)):
        exit(f"Valid -o/--outdir required. '{args.outdir}' does not exist or is not accessible!")

    if not (os.path.exists(args.failures) and os.path.isfile(args.failures)):
        exit(f"Valid -f/--failures required. '{args.failures}' does not exist or is not accessible!")

    return args


def get_copr_builds(copr, dirname=None):
    # Collect all build info
    command = ["copr", "monitor", copr, "--output-format", "json", "--fields", "state,build_id,name,pkg_version,url_build_log"]
    if dirname:
        command.append('--dirname')
        command.append(dirname)

    output = subprocess.run(command, capture_output=True)
    if output.stderr:
        exit(f"[ERROR] ['copr monitor' failure] {output.stderr}")

    return json.loads(output.stdout)


def get_build_log(url, attempts=3):
    if attempts > 0:
        try:
            res = requests.get(url)
            if not res.status_code == 200:
                raise Exception("Request failed.")
            return res.text
        except Exception:
            sleep(0.5)
            get_build_log(url, attempts=attempts-1)
    else:
        exit(f"Failed to collect the build log from: {url}")


def find_known_errors(build_text, known_error_matches):
    known_errors = []
    for regex in KNOWN_ERROR_MATCHES:
        known_errors += regex.findall(build_text)
    return known_errors


def find_potential_errors(build_text, error_line_match, max_error_line_length):
    potential_errors = []
    lines = build_text.split('\n')

    for i, line in enumerate(lines):
        if len(line) > max_error_line_length:
            continue

        elif error_line_match.search(line):
            potential_errors.append(line)

    return potential_errors


def format_build_errors(name, version, url, known_errors, potential_errors):
    return f"""
{'# PACKAGE #'}
{name}=={version}
{url}

{'# KNOWN ERRORS #'}
{f'{NL}'.join(known_errors) if known_errors else ''}

{'# POSSIBLE ERRORS #'}
{f'{NL}'.join(potential_errors) if potential_errors and not known_errors else '# N/A - SEE ABOVE #' if known_errors else ''}

{SEP}"""


def write_results(builds, failed_builds, build_errors, base_path):
    # Write output and display filenames
    builds_filename = f'{base_path}_builds.json'
    failures_filename = f'{base_path}_failures.json'
    errors_filename = f'{base_path}_errors.txt'

    for f, o in [
        (builds_filename, json.dumps(builds, indent=4)),
        (failures_filename, json.dumps(failed_builds, indent=4)),
        (errors_filename, build_errors)
    ]:
        with open(f, 'w') as fp:
            fp.write(o)

    print(dedent(f"""
        Builds:
        {builds_filename}

        Failures:
        {failures_filename}

        Errors:
        {errors_filename}
    """))


def main(copr, outdir, dirname=None, failures_file=None):
    builds_filter = None
    if failures_file:  # Load file as failed_builds
        with open(failures_file) as fp:
            builds_filter = fp.read().splitlines()

        if not builds_filter:
            exit(f"[ERROR] [parsing --failures] No packages found in '{failures_file}'")

    builds = get_copr_builds(copr, dirname)

    failed_builds = [
        build for build in builds if build['state'] == 'failed' and build['pkg_version'] is not None
    ]

    # Rebuild list with only failures we are interested in
    if builds_filter:  # Load file as failed_builds
        filtered_failures = []

        for i, package in enumerate(failed_builds):
            # Search for {package} and {package}=={version} in the file
            if any([
                package['name'] in builds_filter,
                f"{package['name']}=={package['pkg_version']}" in builds_filter
            ]):
                filtered_failures.append(package)

        failed_builds = filtered_failures

    # Build report
    build_errors = [SEP]
    for build in failed_builds:
        build_text = get_build_log(build['url_build_log'])
        known_errors = find_known_errors(build_text, KNOWN_ERROR_MATCHES)
        potential_errors = find_potential_errors(build_text, ERROR_LINE_MATCH, MAX_ERROR_LINE_LENGTH)
        build_errors.append(format_build_errors(
            build['name'], build['pkg_version'], build['url_build_log'], known_errors, potential_errors
        ))
    build_errors = "\n".join(build_errors)  # Convert build_errors list to string

    write_results(builds, failed_builds, build_errors, base_path=f"{outdir}/{copr}")


if __name__ == "__main__":
    args = validate_args(get_args())
    main(args.copr, args.outdir, args.dirname, args.failures)
