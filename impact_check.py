"""
System Requirements:
- `fedpkg` or specify --name
- `git` or specify --branch

Run from the intended rpm directory and branch:
`python impact_check.py -n python-markupsafe --distname python-markupsafe`
"""

import argparse
import json
import subprocess
from time import sleep
from multiprocessing import Pool


def get_args():
    parser = argparse.ArgumentParser(
        prog="python impact_check.py",
        description="Perform a COPR impact check for a package."
    )
    # Unable to be automated (yet?)
    parser.add_argument('--distname', action='store', help="The distname of the package to impact check eg. python-CacheControl => --dist python3-CacheControl", required=True)
    # Automated or optional
    parser.add_argument('-n', '--name', action='store', help="The name of the package to impact check", default=None)
    parser.add_argument('-b', '--branch', action='store', help="The git branch to build against")
    parser.add_argument('--namespace', action='store', help="The src namespace to look for the distgit")
    parser.add_argument('--srpm', action='store', help="The filename of the main srpm to build in copr (alternative to --distgit option)", default=None)
    parser.add_argument('--distgit', action='store_true', help="A flag to utilize your current repo/branch via distgit (alternative to --srpm option)", default=None)
    parser.add_argument('-r', '--repo', action='store', help="The repo to build against", default='http://kojipkgs.fedoraproject.org/repos/rawhide/latest/$basearch/')
    parser.add_argument('-c', '--chroot', action='store', help="The chroot(s) to build against", default='fedora-rawhide-x86_64')
    parser.add_argument('-d', '--delete', action='store', help="The number of days before copr deletion", default='30')
    parser.add_argument('--rebuild', action='store', help="Whether to force package rebuilds despite previous successes", default=False)
    # TODO # parser.add_argument('-f', '--fedora', action='store', help="The (non rawhide) fedora version to build against")
    return parser.parse_args()


def pkgname(line):
    """
    Borrowed from the fedora-packager implementation:
    'Created by Miro Hrončok, with suggestions from Adam Williamson.'
    """
    return line.rsplit("-", 2)[0]


def get_fedpkg_package_name():
    nevra = subprocess.run('fedpkg --release rawhide verrel'.split(' '), capture_output=True)
    if nevra.stderr:
        raise Exception(f"[ERROR] [fedpkg] {nevra.stderr}")

    pkgname = subprocess.run('pkgname', input=nevra.stdout, capture_output=True)
    if pkgname.stderr:
        raise Exception(f"[ERROR] [pkgname] {pkgname.stderr}")

    return pkgname.stdout.decode('utf-8').strip()


def get_git_branch_name():
    proc = subprocess.run('git branch --show-current'.split(' '), capture_output=True)
    if proc.stderr:
        raise Exception(f"[WARNING] [branch name] Attempted to gather branch name. Error: {proc.stderr}")
    return proc.stdout.decode('utf-8').strip()


def get_copr_whoami():
    proc = subprocess.run('copr whoami'.split(' '), capture_output=True)
    if proc.stderr:
        raise Exception(f"[ERROR] [namespace] {proc.stderr}")
    return proc.stdout.decode('utf-8').strip()


def copr_exists(copr_name):
    return subprocess.run(['copr', 'get', copr_name], capture_output=True).returncode == 0


def get_dependencies(distname, recursive=True):
    cmd = ['repoquery', '--repo', 'rawhide,rawhide-source', '--whatrequires', distname]
    if recursive:
        cmd.append('--recursive')
    packages = cmd_output(cmd).splitlines()
    sources = list(map(pkgname, sorted(list(set(filter(lambda s: s.endswith('src'), packages))))))
    return sources


def run_command(cmd):
    proc = subprocess.run(cmd, capture_output=True)
    return proc.returncode, proc.stderr.decode('utf-8').strip(), proc.stdout.decode('utf-8').strip()


def cmd_bool(cmd):
    ret, stderr, stdout = run_command(cmd)
    if ret != 0:
        print(f"Command Failure:\n\t{' '.join(cmd)}\n\t->\n\t{stderr}")
        return False
    return True


def cmd_output(cmd):
    ret, stderr, stdout = run_command(cmd)
    if ret != 0:
        print(f"Command Failure:\n\t{' '.join(cmd)}\n\t->\n\t{stderr}")
    return stdout


def copr_create(copr, repo, chroot, delete):
    return cmd_bool([
        'copr', 'create', copr,
        '--repo', repo,
        '--chroot', chroot,
        '--delete-after-days', delete
    ])


def add_package_distgit(copr, package, commit='rawhide', rebuild='on', namespace=None):
    cmd = [
        'copr', 'add-package-distgit', copr,
        '--name', package,
        '--commit', commit,
        '--webhook-rebuild', 'on' if rebuild else 'off'
    ]

    if namespace:
        cmd.append('--namespace')
        cmd.append(namespace)

    return cmd_bool(cmd)


def add_packages_distgit(copr, packages):
    with Pool() as pool:
        results = pool.starmap(add_package_distgit, [(copr, package) for package in packages])

    failures = []
    for i, result in enumerate(results):
        if result is False:
            failures.append(packages[i])

    for failure in failures:
        print(f"FAILED TO ADD VIA DISTGIT: {failure}")

    return failures


def build_source(copr, package_source):
    return cmd_bool([
        'copr', 'build', copr, package_source
    ])


# Note: dirname param not applicable as build-package relies on the copr name to have the build dir
def build_package(copr, package):
    return cmd_bool([
        'copr', 'build-package', copr, '--name', package
    ])


def build_package_async(copr, package):
    return cmd_bool([
        'copr', 'build-package', copr, '--name', package, '--background', '--nowait'
    ])


def build_packages(copr, packages, source=False, nowait=False):
    if source and nowait:
        print("build_packages should not be called with source and async. Running with source=True and nowait=False.")

    if source:
        with Pool() as pool:
            results = pool.starmap(build_source, [(copr, package) for package in packages])

    elif nowait:
        with Pool() as pool:
            results = pool.starmap(build_package_async, [(copr, package) for package in packages])

    else:
        with Pool() as pool:
            results = pool.starmap(build_package, [(copr, package) for package in packages])

    failures = []
    for i, result in enumerate(results):
        if result is False:
            failures.append(packages[i])

    for failure in failures:
        print(f"FAILED TO BUILD/START BUILD FOR: {failure}")

    return failures


def get_all_builds(copr):
    """
    Provides a CLI based parse of all builds (something the API doesn't provide)
    NOTE: known potential race condition where webhook rebuild completes before initial subdir build
    NOTE: known potential issue where non relevant webhook build passes but our subdir build fails
          (copr monitor used in authoritative cases, this function used for general purpose 'completeness' check)
    Returns:
        {
            package name: {
                build id: build status
            },
            ...
        }
    """
    builds = dict()
    build_text = cmd_output(['copr', 'list-builds', '--output', 'json', copr])

    for build in json.loads(build_text):
        bid, name, state = build['id'], build['name'], build['state']
        if not builds.get(name):
            builds[name] = {bid: state}
        else:
            builds[name][bid] = state

    return builds


def copr_monitor(copr, dirname=None):
    cmd = ['copr', 'monitor', '--output', 'json', copr]
    if dirname:
        cmd.append(f'--dirname={dirname}')
    return cmd_output(cmd)


def get_packages(copr):
    cmd = ['copr', 'list-package-names', copr]
    return cmd_output(cmd).splitlines()


def get_latest_builds(copr, dirname=None):
    """
    Returns:
        {
            package name: build status,
            ...
        }
    """
    builds = dict()
    build_text = copr_monitor(copr, dirname)

    if build_text:
        for build in json.loads(build_text):
            bid, name, state = build['build_id'], build['name'], build['state']
            builds[name] = state

    return builds


def find_incomplete_builds(latest_builds, all_builds):
    complete_states = ['succeeded', 'failed']
    incomplete_builds = []

    for package, state in latest_builds.items():
        # Find any incomplete builds
        if state not in complete_states:
            if len(all_builds[package]) == 1:
                # The latest build is the only build
                incomplete_builds.append(package)
            else:
                # Not all of the builds have completed yet
                if not all([state in complete_states for state in all_builds[package].values()]):
                    incomplete_builds.append(package)

    return incomplete_builds


def poll_for_incompletes(copr, dirname):
    attempt = 0
    retries = 35
    sleep_timers = [1, 3, 7, 15] + [60] * retries  # Leave trailing entries for simple modification
    while attempt <= retries:
        incomplete_builds = find_incomplete_builds(
            get_latest_builds(copr, dirname=dirname),
            get_all_builds(copr)
        )
        if incomplete_builds:
            print(f"[Attempt #{attempt}] Incomplete builds remain. Sleeping for {sleep_timers[attempt]} minutes.")
            sleep(sleep_timers[attempt] * 60)
            attempt += 1
        else:
            break

    if attempt > retries:
        print(f"Maximum retries ({retries}) exceeded. Incomplete builds still remain. Please manually investigate.")
        print("\n".join(incomplete_builds))
        return incomplete_builds


# Idempotent, User-Friendly Functions
def attempt_get_dependencies(distname):
    transitive_dependencies = get_dependencies(distname, recursive=True)
    direct_dependencies = get_dependencies(distname, recursive=False)

    print()
    if not transitive_dependencies:
        print(f"{distname} has no dependencies. Ensure correct spelling of the distname otherwise no impact check required.")
        exit(1)
    else:
        print(f"(For reference) Direct dependencies for {package_name}:")
        print("\n".join(direct_dependencies))
        print("\n" + f"(For builds) Transitive dependencies for {package_name}:")
        print("\n".join(transitive_dependencies))

    return transitive_dependencies


def attempt_copr_create(copr, repo, chroot, delete):
    print()
    if copr_exists(copr):
        print(f"COPR Exists, skipping creation: {copr}")
    else:
        print(f"Creating COPR: {copr}")
        copr_create(copr, repo, chroot, delete)


if __name__ == "__main__":
    args = get_args()

    srpm = args.srpm
    package_name = args.name or get_fedpkg_package_name()
    namespace = args.namespace or f"forks/{get_copr_whoami()}"

    copr = f'autoimpact-{package_name}'
    copr_isolated_dir = f'{copr}:custom:isolated'
    copr_control = f'autoimpact-{package_name}-control'
    copr_control_isolated_dir = f'{copr_control}:custom:isolated'

    dependencies = attempt_get_dependencies(args.distname)

    attempt_copr_create(copr, args.repo, args.chroot, args.delete)
    attempt_copr_create(copr_control, args.repo, args.chroot, args.delete)

    current_packages = get_packages(copr)

    # Add primary package
    print()
    if package_name in current_packages:
        print(f"Primary package ({package_name}) was already successfully added.")
    elif srpm:
        print(f"Primary package ({package_name}) has not yet been added. The SRPM upload will add and build together.")
    else:
        print(f"Adding primary package ({package_name}) via distgit.")
        branch_name = args.branch or get_git_branch_name()
        if not add_package_distgit(copr, package_name, commit=branch_name, rebuild='on', namespace=namespace):
            exit(f"\tPrimary package ({package_name}) experienced an error while being added to copr.")

    # Add dependencies
    print()
    missing_dependencies = [dep for dep in dependencies if dep not in current_packages]
    if missing_dependencies:
        print(f"Found {len(missing_dependencies)} dependencies to add to the COPR:")
        print("\n".join(missing_dependencies))
        add_packages_distgit(copr, missing_dependencies)

    # Build primary package
    print()
    try:
        primary_builds = get_latest_builds(copr)
    except Exception:  # No builds yet
        primary_builds = {}

    primary_package_built = args.rebuild is False and primary_builds.get(package_name) == 'succeeded'
    if primary_package_built:
        print(
            f"Primary package ({package_name}) was already successfully built. "
            "To force a rebuild, provide the --rebuild flag (srpm case) or "
            "push updates to the given repo (dist-git case)."
        )
    elif srpm:  # (re)build via srpm
        print(f"Adding and building primary package ({package_name}) via SRPM: {srpm}")
        build_source(copr, srpm)
    else:  # (re)build via dist-git
        print(f"Building primary package ({package_name}) via distgit.")
        if not build_package(copr, package_name):
            print(f"[ERROR] [Build Primary Package] {package_name} failed to build, can not continue!")
            exit(1)

    # Build Dependencies
    print()
    if args.rebuild:
        to_build = dependencies
    else:
        try:
            builds = get_latest_builds(copr, dirname=copr_isolated_dir)
            failed_builds = [k for k, v in builds.items() if v == 'failed']
            missing_builds = [dep for dep in dependencies if dep not in builds.keys()]
            to_build = failed_builds + missing_builds
        except Exception:
            # If dirname does not exist yet, get_latest_builds will throw
            to_build = dependencies

    print(f"Found {len(to_build)} packages to build in the COPR:")
    print("\n".join(to_build))
    build_packages(copr_isolated_dir, to_build, nowait=True)
    sleep(60)  # Allow one minute for the packages to show in COPR

    # sleep/check until all dependency builds have settled
    print()
    incomplete_builds = poll_for_incompletes(copr, copr_isolated_dir)
    if incomplete_builds:
        exit("Unable to complete all builds in copr. Investigate builds manually:" + "\n".join(incomplete_builds))

    # Get failures
    print()
    builds = get_latest_builds(copr, dirname=copr_isolated_dir)
    failed_builds = [k for k, v in builds.items() if v == 'failed']
    print("Failed Builds:")
    print('\n'.join(failed_builds))

    ############## FLIP TO CONTROL #################

    # Add/build failures in control
    print()
    if failed_builds:
        print(f"Found {len(failed_builds)} failures to build in the control COPR:")
        print("\n".join(failed_builds))
        add_packages_distgit(copr_control, failed_builds)
        build_packages(copr_control_isolated_dir, failed_builds, nowait=True)
        sleep(60)  # Allow one minute for the packages to show in COPR
        print()
        incomplete_builds = poll_for_incompletes(copr_control, copr_control_isolated_dir)
        if incomplete_builds:
            exit("Unable to complete all builds in control copr. Investigate builds manually:" + "\n".join(incomplete_builds))

    print()
    builds = get_latest_builds(copr_control, dirname=copr_control_isolated_dir)
    failed_control_builds = [k for k, v in builds.items() if v == 'failed']
    print("Control COPR Failed Builds:")
    print('\n'.join(failed_control_builds))

    ############## ANALYZE RESULTS #################

    print()
    failed_on_both = []
    for build in failed_builds:
        if build in failed_control_builds:
            failed_on_both.append(build)
    print("Failed on both, likely not due to your changes:")
    print('\n'.join(failed_on_both))

    print()
    failed_on_latest = []
    for build in failed_builds:
        if build not in failed_control_builds:
            failed_on_latest.append(build)
    print("Failed on latest, Passed on control:")
    print('\n'.join(failed_on_latest))

    # TODO: Incorporate fail-to-add/fail-to-build packages into final analysis
    if failed_on_latest:
        print("Impact check failed! Some packages have failed to build that were not failing before!")
    else:
        print("Impact check passed - no anomalies found!")